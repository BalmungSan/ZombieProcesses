# ProcessControl

Práctica final Sistemas Operativos (ST0257) 2016-2

## Profesores:
* Juan Francisco Cardona McCormick
* Juan David Pineda Cárdenas

## Team UndeadOs
* Luis Miguel Mejía Suárez
* Alejandro Salgado Gómez

## How to use it
### Control Console

	conctrl [--ficheroconfiguracion=<config_file>] [--semaforo=<id>] [--memoriacompartida=<id>]

**Where:**

* ficheroconfiguracion: is the path to the configuration file. if no given the deafult value of "conctrl.cfg" is used.
* semaforo: is the id for the posix semaphore to create. If no given the default value of "conctrlsem" is used.
* memoriacompartida: is the id for the posix shared memory to create. If no given the default value of "conctrlmem" is used.

### Control Process

	procesoctrl --filepath=<path> --filename=<file> --reencarnacion=<number> [--semaforo=<id>] [--memoriacompartida=<id>] id
	
**Where:**
* filepath: is the path where the suicidal program lives. Must be passed
* filename: is the name of the suicidal program exe. Must be passed
* reencarnacion: is the number of lives for the suicidal process (0 means infinite lives). Must be passed and be >= 0
* semaforo: is the id for the posix semaphore to open. If no given the default value of "conctrlsem" is used.
* memoriacompartida: is the id for the posix shared memory to open. If no given the default value of "conctrlmem" is used.
* id: is the id for this Control Process. Must be passed

## Install
Read the 'Install' file

## Dependencies
* C++11 (gcc >= 4.8.5)

## Notes
* The id of the process must be less or equal than 32 characters.
* Send commands to a process that was suspended is not supported. (Except the
  command "restableccer")
* Memory commands to get iformation about deaths are supported.
* If a process is dead the only valid command that can be sended is deaths.