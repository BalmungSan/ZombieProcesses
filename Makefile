init: all
	mkdir -p ./bin
	mv ./src/conctrl ./bin
	mv ./src/procesoctrl ./bin

all: src/conctrl src/procesoctrl

src/conctrl.o: src/conctrl.cpp src/memstruct.h src/threadproctrl.h
	g++ -Wall --std=c++11 -g -c $< -o $@

src/threadproctrl.o: src/threadproctrl.cpp src/threadproctrl.h
	g++ -Wall --std=c++11 -g -c $< -o $@

src/conctrl: src/conctrl.o src/threadproctrl.o
	g++ -Wall -pthread -lrt -g $^ -o $@

src/procesoctrl: src/procesoctrl.cpp src/memstruct.h
	g++ -Wall --std=c++11 -pthread -lrt -g -o $@ $<

install: all
	mkdir -p $(PREFIX)/bin
	mv ./src/conctrl $(PREFIX)/bin
	mv ./src/procesoctrl $(PREFIX)/bin
	cp -r ./examples $(PREFIX)

clean:
	rm -rf ./bin
	rm -f ./src/*.o
	rm -f ./src/conctrl
	rm -f ./src/procesoctrl
