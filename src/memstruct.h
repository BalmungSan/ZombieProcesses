#pragma once
#include <string>
using namespace std;

/**
 * This structure coxntains the information
 * of all deaths of one suicidal process
 */
struct InfoMuerte {
  char id[32]; //the unique id of this suicidal process
  int nDecesos; //the number of deaths of this process
};

/**
 * This structure contains the information
 * of all deaths of the suicidal processes
 * and is shared across one conctrl
 * and all instances of procesoctrl
 * asociated with that conctrl
 */
struct MemoriaCompartida {
  int n; //number of procesoctrl instances
  long int valSeq; //the total number of deaths of all suicidal processes
  InfoMuerte* muertes; //each entry is a suicidal process, must be size n
};
