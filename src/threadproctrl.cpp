#include <ext/stdio_filebuf.h>
#include <sys/wait.h>
#include "threadproctrl.h"

/**
 * Use an initialization list
 */
ThreadProctrl::ThreadProctrl(string path, string file, string lives, string suiID, int id):
  path(path), file(file), lives(lives), suiID(suiID), id(id), killed(false) {
  //prepare pipes
  if (pipe(pipein) == -1) fail();
  if (pipe(pipeout) == -1) fail();
  if (pipe(pipeerr) == -1) fail();
}

/**
 * Listen to the output of procesoctrl
 * and writes to the standar output of conctrl
 * @note the output of procesoctrl is written to the standar error
 */
void ThreadProctrl::listening() {
  __gnu_cxx::stdio_filebuf<char> errbuf(pipeerr[0], ios::in);
  istream stderr(&errbuf);

  bool run = true;
  string outline;
  do {
    getline(stderr, outline);
    if (outline.compare("terminar") == 0) {
      if (!killed) {
        sendCommand("terminar");
        deleteControlProcess(suiID);
      }

      run = false;
    } else {
      sem_wait(consem);
      cout << endl << outline << endl;
      cout << "conctrl> ";
      cout.flush();
      sem_post(consem);
    }
  } while(run);

  errbuf.close();
}

/**
 * Sends commands to procesoctrl
 * and writes the output to the standar output of conctrl
 * @note the output of the commands is written to the standar output
 */
void ThreadProctrl::sending() {
  __gnu_cxx::stdio_filebuf<char> inbuf(pipein[1], ios::out);
  ostream stdin(&inbuf);
  __gnu_cxx::stdio_filebuf<char> outbuf(pipeout[0], ios::in);
  istream stdout(&outbuf);

  bool run = true;
  string outline;
  unique_lock<mutex> lk(m, defer_lock);
  do {
    //wait for a command
    lk.lock();
    cv.wait(lk, [this]{return ready;});

    //check if the procesoctrl died
    if (command.compare("terminar") == 0) run = false;

    //send the command
    stdin << command << endl;
    getline(stdout, outline);
    sem_wait(consem);
    cout << endl << outline << endl;
    cout << "conctrl> ";
    cout.flush();
    sem_post(consem);

    ready = false;
    lk.unlock();
  } while(run);

  inbuf.close();
  outbuf.close();
}

/**
 * Starts the thread
 */
void ThreadProctrl::start() {
  //execute procesoctrl
  bool failed = false;
  pid_t proctrl = fork();
  if (proctrl == -1) fail();
  else if (proctrl == 0) {
    //close unused pipe sides for procesoctrl
    if (close(pipein[1]) == -1) fail();
    if (close(pipeout[0]) == -1) fail();
    if (close(pipeerr[0]) == -1) fail();

    //duplicate pipes as standar streams for procesoctrl
    if (dup2(pipein[0], 0) == -1) fail();
    if (dup2(pipeout[1], 1) == -1) fail();
    if (dup2(pipeerr[1], 2) == -1) fail();

    //exec procesoctrl
    execl("./bin/procesoctrl", "procesoctrl", ("--filepath="+path).c_str(),
          ("--filename="+file).c_str(), ("--reencarnacion="+lives).c_str(),
          ("--mem="+sharedmem).c_str(), ("--sem="+semaphore).c_str(),
          (to_string(id).c_str()), (char*) NULL);

    //if we are here exec fail
    failed = true;
    _exit(1);
  }

  if (failed) fail();

  //close unused pipe sides for thread
  if (close(pipein[0]) == -1) fail();
  if (close(pipeout[1]) == -1) fail();
  if (close(pipeerr[1]) == -1) fail();

  tlistener = thread(&ThreadProctrl::listening, this);
  tcommand = thread(&ThreadProctrl::sending, this);
}

/**
 *
 */
void ThreadProctrl::sendCommand(string command) {
  unique_lock<mutex> lk(m);
  this->command = command;
  this->ready = true;
  cv.notify_one();
}

/**
 * Stops the thread
 * This function blocks until the thread has sucefully stoped
 */
void ThreadProctrl::stop() {
  //kill this procesoctrl
  sendCommand("terminar");
  killed = true;

  //wait threads
  tlistener.join();
  tcommand.join();

  //check status
  int status;
  if (waitpid(proctrl, &status, 0) == -1) fail();
}
