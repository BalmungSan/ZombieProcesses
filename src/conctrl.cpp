#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include "memstruct.h"
#include "threadproctrl.h"
using namespace std;

//variables
string confgFile = "conctrl.cfg";
string semaphore = "conctrlsem";
string sharedmem = "conctrlmem";

//options
const option options[] = {{"ficheroconfiguracion", 1, 0, 'c'},
                          {"semaforo", 1, 0, 's'},
                          {"memoriacompartida", 1, 0, 'm'},
                          {"help", 0, 0, 'h'},
                          {0, 0, 0, 0}}; // to avoid segfaults

//control process
int total = 0;
map<string,ThreadProctrl*> controlProcesses;
map<string,int> ids;

//shared resorces
MemoriaCompartida* mem; //shared memory
off_t size;             //size of the shared memory
int fd;                 //file descriptor of the shared memory
sem_t* consem;          //semaphore for threads to print on console
sem_t* memsem;          //semaphore for porcess to acces the shared memory

//commands
enum class Commands {OTHER, END, DEATHS, ALLDEATHS};
map<string,Commands> commands = {{"terminar", Commands::END},
                                 {"deaths", Commands::DEATHS},
                                 {"alldeaths", Commands::ALLDEATHS}};

/**
 * Prints and error menssage and exit
 */
void fail() {
  cerr << "Fail due: " << strerror(errno) << endl;
  exit(2);
}

/**
 * Prints usage info
 */
void usage (char* program) {
  cerr << "use:" << endl
       << program << endl
       << "\t[--ficheroconfiguracion=<config_file>] "
       << "to specify the configuration file (default 'conctrl.cfg')" << endl
       << "\t[--semaforo=<id>] "
       << "to specify the semaphore id (default 'conctrlsem')" << endl
       << "\t[--memoriacompartida=<id>] "
       << "to specify the shared memory id (default 'conctrlmem')" << endl
       << program << " --help to print this message again" << endl;
  exit(1);
}

/**
 * Analyzes the arguments using getopt
 */
void getOptions (int argc, char* argv[]) {
  int index; //not used, but can be helpful for debugging
  int o;

  //iterate over all arguments
  do {
    o = getopt_long(argc, argv, "", options, &index);

    switch(o) {
    case -1: //ignore
      break;

    case 'c': //set the config file
      confgFile.assign(string(optarg));
      break;

    case 's': //set the semaphore
      semaphore.assign(string(optarg));
      break;

    case 'm': //set the sharedmemory
      sharedmem.assign(string(optarg));
      break;

    default: //bad option or help
      usage(argv[0]);
      break;
    }
  } while (o != -1);
}

/**
 * Prints an error message if a bad token found while parsing the file
 */
void parseError(const char* token, const char* expected) {
  cerr << "Error reading the configuration file" << endl
       << "bad token: " << token << endl
       << "expected: " << expected << endl
       << "configuration file: " << confgFile << endl;
  exit(3);
}

/**
 * Parses the configuration file to create all the control processes
 * This method sets the global variable total
 * as the total number of control process to create
 */
void parseFile() {
  //read the configuration file
  char* buffer;
  int length;
  ifstream cfg(confgFile);
  if (cfg) {
    cfg.seekg(0, ios::end);
    length = cfg.tellg();
    cfg.seekg(0, ios::beg);
    buffer = new char[length];
    cfg.read(buffer, length);
    cfg.close();
  } else {
    cerr << "Error opening the file: " << confgFile << endl;
    exit(3);
  }

  //parse the file using strtok
  char wp[] = " \t\r\n\v\f";
  char* token = strtok(buffer, wp);
  char *id, *path, *file, *lives;
  do {
    //'ProcesoSui'
    if (strcmp(token, "ProcesoSui") != 0) parseError(token, "ProcesoSui");

    //ID
    id = strtok(NULL, wp);
    if (id == NULL) parseError("[EOF]", "[ID]");

    //'{'
    token = strtok(NULL, wp);
    if (token == NULL) parseError("[EOF]", "{");
    if (strcmp(token, "{") != 0) parseError(token, "{");

    //path
    path = strtok(NULL, wp);
    if (path == NULL) parseError("[EOF]", "[PATH]");

    //'::'
    token = strtok(NULL, wp);
    if (token == NULL) parseError("[EOF]", "::");
    if (strcmp(token, "::") != 0) parseError(token, "::");

    //file
    file = strtok(NULL, wp);
    if (file == NULL) parseError("[EOF]", "[FILE]");

    //lives
    lives = strtok(NULL, wp);
    if (lives == NULL) parseError("[EOF]", "[POSITIVE_NUMBER]");
    for (char* c = lives; *c; c++) if (!isdigit(*c)) parseError(lives, "[POSITIVE_NUMBER]");

    //'}'
    token = strtok(NULL, wp);
    if (token == NULL) parseError("[EOF]", "}");
    if (strcmp(token, "}") != 0) parseError(token, "}");

    //create a new process thread
    ids[id] = total;
    controlProcesses[id] = new ThreadProctrl(path, file, lives, id, total++);

    //start again
    token = strtok(NULL, wp);
  } while (token != NULL); //each iteration is a ProcesoSui
}

/**
 * Start the execution of all control process threads
 */
void start() {
  int i = 0;
  for (auto cp : controlProcesses) {
    strcpy(mem->muertes[i].id, cp.first.c_str());
    cp.second->start();
    i++;
  }
}

/**
 * Stop the execution of all control process threads
 * This method delete all ThreadProctrl instances
 */
void stop() {
  for (auto cp : controlProcesses) cp.second->stop();
  controlProcesses.clear();
}

/**
 * Creates the sahred memory and the posix semaphore
 * This method uses the global variable total
 * for the size of the shared memory
 */
void initShared() {
  //create the shared memory
  fd = shm_open(sharedmem.c_str(), O_CREAT | O_EXCL | O_RDWR, 0660);
  if (fd == -1) fail();

  //set the size of the shared memory
  size = sizeof(MemoriaCompartida) + sizeof(InfoMuerte) * total;
  if (ftruncate(fd, size) == -1) fail();

  //assing the shared memory to the structure
  mem = (MemoriaCompartida*) mmap(NULL, size, PROT_READ | PROT_WRITE,
                                  MAP_SHARED, fd, 0);
  if (mem == MAP_FAILED) fail();

  mem->n = total;
  mem->valSeq = 0;
  mem->muertes = (InfoMuerte*) (mem + 1);

  //memory semaphore
  memsem = sem_open(semaphore.c_str(), O_CREAT | O_EXCL, 0660, 1);
  if (memsem == SEM_FAILED) fail();

  //threads semaphore
  consem = new sem_t();
  if ((sem_init(consem, 0, 1)) == -1) fail();
}

/**
 * destroy all shared resources
 */
void destroyShared() {
  //shared memory
  if (munmap(mem, size) == -1) fail();
  if (close(fd) == -1) fail();
  if (shm_unlink(sharedmem.c_str()) == -1) fail();

  //memory semaphore
  if (sem_close(memsem) == -1) fail();
  if (sem_unlink(semaphore.c_str()) == -1) fail();


  //threads semaphore
  if (sem_destroy(consem) == -1) fail();
  delete consem;
}

/**
 * prompt for the user type commands
 */
void prompt(){
  string cmd;
  char wp[] = " \t\v\f";

  //read user commands
  cout << "conctrl> ";
  cout.flush();
  while (getline(cin, cmd)) {
    //ignore empty line
    if (cmd.empty()) {
      cout << "conctrl> ";
      cout.flush();
      continue;
    }

    //execute command
    string command(strtok(strdup(cmd.c_str()), wp));
    char* i = strtok(NULL, wp);
    string id(i ? i : "");
    char* n = strtok(NULL, wp);
    string number(n ? n : "");

    switch (commands[command]) {
    case Commands::END :
      //terminate procesoctrl
      if (id.compare("*") == 0) {
        //all process
        for (auto cp : controlProcesses) cp.second->stop();
        controlProcesses.clear();
      } else if (controlProcesses.find(id) == controlProcesses.end()) {
        //bad id
        cerr << "bad id: " << id << endl;
      } else {
        //one process
        controlProcesses[id]->stop();
        controlProcesses.erase(id);
      }

      break;

    case Commands::DEATHS :
      //get deaths from procesoctrl
      if (id.compare("*") == 0) {
        //all process
        for (auto cp: ids)
          cout << "deaths for procesoctrl with id '"
               << cp.second << "': "
               << mem->muertes[cp.second].nDecesos
               << endl;
      } else if (ids.find(id) == ids.end()) {
        //bad id
        cerr << "bad id: " << id << endl;
      } else {
        //one process
        cout << "deaths for procesoctrl with id '"
             << ids[id] << "': "
             << mem->muertes[ids[id]].nDecesos
             << endl;
      }

      break;

    case Commands::ALLDEATHS :
      //get all deaths
      cout << "total deaths: "
           << mem->valSeq
           << endl;
      break;

    case Commands::OTHER :
      //send other commands to procesoctrl
      if (id.compare("*") == 0) {
        //all process
        for (auto cp : controlProcesses) cp.second->sendCommand(command + " " + number);
      } else if (controlProcesses.find(id) == controlProcesses.end()) {
        //bad id
        cerr << "bad id: " << id << endl;
      } else {
        //one process
        controlProcesses[id]->sendCommand(command + " " + number);
      }

      break;
    }

    cout << "conctrl> ";
    cout.flush();
  }

  cout << "quit" << endl;
}

/**
 * remove an entry from the map of control processes
 * @param id the id of the entry to remove
 */
void deleteControlProcess(string id) {
  controlProcesses.erase(id);
}

/**
 * Main function
 * all code logic runs here
 */
int main (int argc, char* argv[], char** env) {
  //get options
  getOptions(argc, argv);

  //parse the config file
  parseFile();

  //create the shared memory and the semaphore
  initShared();

  //start the execution of the process control threads
  start();

  //read commands
  prompt();

  //stop the process control threads
  stop();

  //destroy all shared resources and exit
  destroyShared();
  return 0;
}
