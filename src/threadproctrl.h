#pragma once
#include <fstream>
#include <thread>
#include <iostream>
#include <mutex>
#include <condition_variable>
#include <unistd.h>
#include <semaphore.h>
using namespace std;

//externs
extern string semaphore;
extern string sharedmem;
extern sem_t* consem;
extern void fail();
extern void deleteControlProcess(string id);

/**
 * Thread to connect conctrl
 * with a procesoctrl
 */
class ThreadProctrl {
private:
  thread tcommand;  //a thread for sending commands to procesoctrl
  thread tlistener; //a thread for listening to the output of procesoctrl
  pid_t proctrl;    //the pid of the procesoctrl instance

  string path;  //the path to pass to procesoctrl
  string file;  //the file to pass to procesoctrl
  string lives; //the amount of lives to pass to procesoctrl
  string suiID; //the id of the suicidal process
  int id;       //the id to pass to procesoctrl
  bool killed;  //flag to determine when the procesoctrl was killed or not

  mutex m;               //mutex for sending commands
  condition_variable cv; //condition variable for sending commands
  bool ready = false;    //boolean for the condition variable
  string command;        //the command to send to procesoctrl

  int pipein[2];    //pipe to standar in of procesoctrl
  int pipeout[2];   //pipe from standar out of procesoctrl
  int pipeerr[2];   //pipe from standar error of procesoctrl
  void listening(); //this function listen to the output of procesoctrl in parallel
  void sending();   //this function sends commands to procesoctrl in parallel

public:
  ThreadProctrl(string path, string file,string lives, string suiID, int id); //constructor
  void sendCommand(string command); //send a command to procesoctrl
  void start(); //starts execution
  void stop(); //stops execution
};
