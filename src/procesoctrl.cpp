#include <iostream>
#include <string>
#include <map>
#include <thread>
#include <queue>
#include <fcntl.h>
#include <getopt.h>
#include <semaphore.h>
#include <string.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/wait.h>

#include "memstruct.h"

using namespace std;

#define rd 0
#define wr 1

#define out 1
#define err 2

//variables
string path = "";
string file = "";
string full_path = "";
int lives = -1;
string sharedmem = "contrlmem";
string semaphore = "conctrlsem";
int id;
bool infinite; // if the process has infinite lives
bool paused; // if the process is paused

int fd; //file descriptor to map the shared memory
int size; //shared memory size
MemoriaCompartida* mem; //shared memory
InfoMuerte * info; //pointer to process structure
sem_t* memsem; //semaphore for porcess to acces the shared memory

//arrays for pipes
int output[2];
int error[2];

//options
const option options[] = {{"filepath", 1, 0, 'p'},
                          {"filename", 1, 0, 'f'},
                          {"reencarnacion", 1, 0, 'l'},
                          {"semaforo", 1, 0, 's'},
                          {"memoriacompartida", 1, 0, 'm'},
                          {"help", 0, 0, 'h'},
                          {0, 0, 0, 0}}; // to avoid segfaults

//commands
queue<string> commands_queue;
enum class Commands {OTHER, LIST, SUM, SUB, PAUSE, CONT, INDF, DEF, END};
map<string,Commands> commands = {{"listar", Commands::LIST},
                                 {"sumar", Commands::SUM},
                                 {"restar", Commands::SUB},
                                 {"suspender", Commands::PAUSE},
                                 {"restablecer", Commands::CONT},
                                 {"indefinir", Commands::INDF},
                                 {"definir", Commands::DEF},
                                 {"terminar", Commands::END}};

/**
 * Prints usage info
 */
void usage (char* program) {
  cerr << "use:" << endl
       << program << endl
       << "\t--filepath=<path> "
       << "to specify the path where the suicidal process lives" << endl
       << "\t--filename=<file> "
       << "to specify the name of the suicidal process"<< endl
       << "\t--reencarnacion=<number> "
       << "to specify the number of lives for the suicidal process "
       << "(0 means infinite lives)" << endl
       << "\t[--semaforo=<id>] "
       << "to specify the semaphore id (default 'conctrlsem')" << endl
       << "\t[--memoriacompartida=<id>] "
       << "to specify the shared memory id (default 'conctrlmem')" << endl
       << "\t<id> the id of this control process" << endl
       << program << " --help to print this message again" << endl;
}

void executeCommand (string cmd) {

  if(cmd.empty()) return;

  string command(strtok(strdup(cmd.c_str()), " "));
  char * i = strtok(NULL, " \n");
  int n = i ? atoi(i) : 0;
  string strlive;

  switch (commands[command]) {
  case Commands::LIST :
    //prints the information of the procesoctrl

    if(infinite) strlive = "Infinito";
    else strlive = to_string(lives);

    cout << "Proceso Control " << id << " -- "
         << "Proceso suicida " << info->id
         << " ha muerto " << info->nDecesos << " veces"
         << ", vidas restantes: " << strlive << endl;

    break;

  case Commands::SUM :
    //sum lives
    if(!infinite){
      lives += n;

      cout << "Proceso Control " << id << " -- "
           << "Proceso suicida " << info->id
           << " vidas = " << lives << endl;
    }
    else{
      cout << "No se puede sumar al infinito" << endl;
    }

    break;

  case Commands::SUB :
    //substract lives
    if(!infinite){
      lives -= n;

      cout << "Proceso Control " << id << " -- "
           << "Proceso suicida " << info->id
           << " vidas = " << lives << endl;
    }
    else{
      cout << "No se puede restar al infinito" << endl;
    }

    break;

  case Commands::PAUSE :
    //pause execution

    cout << "Proceso Control " << id << " -- "
         << "Proceso suicida " << info->id
         << " ha sido suspendido" << endl;

    while(paused);
    break;

  case Commands::CONT :
    // continue
    cout << "Proceso Control " << id << " -- "
         << "Proceso suicida " << info->id
         << " continua" << endl;
    break;

  case Commands::INDF :
    //make lives infinite
    infinite = true;

    cout << "Proceso Control " << id << " -- "
         << "Proceso suicida " << info->id
         << " vidas = Infinito" << endl;

    break;

  case Commands::DEF :
    // make lives finite
    infinite = false;
    lives = n;

    cout << "Proceso Control " << id << " -- "
         << "Proceso suicida " << info->id
         << " vidas = " << lives << endl;
    break;

  case Commands::END :
    // terminate the execution
    infinite = false;
    lives = 0;

    cout << "Proceso Control " << id << " -- "
         << "Proceso suicida " << info->id
         << " termina su ejecucion" << endl;

    break;

  case Commands::OTHER :
    //bad command
    cout << "Bad command: " << cmd << endl;
    break;
  }

}

void initShared() {
  //get the shared memory
  fd = shm_open(sharedmem.c_str(), O_RDWR, 0660);
  if (fd == -1){
    cerr << "Error opening the shared memory" << endl;
  }

  size = sizeof(MemoriaCompartida) + sizeof(InfoMuerte)*(id+1);

  mem = (MemoriaCompartida*) mmap(NULL, size, PROT_READ | PROT_WRITE,
                                  MAP_SHARED, fd, 0);

  if (mem == MAP_FAILED){
    cerr << "Error mapping the shared memory to read n" << endl;
  }

  //memory semaphore
  memsem = sem_open(semaphore.c_str(), O_RDWR);
  if (memsem == SEM_FAILED){
    cerr << "Error opening the semaphore" << endl;
  }

  info = ((InfoMuerte*)(mem + 1)) + id;
}

int createChild(){
  int childpid;

  int fd_null = open("/dev/null", O_WRONLY);

  pipe(output);
  pipe(error);

  if((childpid = fork()) == -1){
    cerr << "Error creating the child in procesoctrl " << id << endl;
  }

  if(childpid == 0){ // child
    close(output[rd]);
    close(error[rd]);
    dup2(output[wr], out);
    dup2(error[wr], err);

    execl(full_path.c_str(), file.c_str(), NULL);
  }
  else{ //father
    dup2(output[rd], fd_null);
    dup2(error[rd], fd_null);
  }

  return childpid;
}

void destroyShared() {
  //shared memory
  if (munmap(mem, size) == -1)
    cerr << "Error trying to unmap the shared memory" << endl;
  if (close(fd) == -1)
    cerr << "Error trying to close the shared memory file descriptor" << endl;

  //pipe
  if(close(output[rd]) == -1){
    cerr << "Error closing the output pipe for read" << endl;
  }
  if(close(output[wr]) == -1){
    cerr << "Error closing the output pipe for write" << endl;
  }
  if(close(error[rd]) == -1){
    cerr << "Error closing the error pipe for read" << endl;
  }
  if(close(error[wr]) == -1){
    cerr << "Error closing the error pipe for write" << endl;
  }

  //memory semaphore
  if (sem_close(memsem) == -1){
    cerr << "Error closing the semaphore" << endl;
  }
}

void commandReader(){
  string cmd;

  bool run = true;

  while(run){

    getline(cin,cmd);
    if(cmd.compare("terminar") == 0)
      run = false;
    else if(cmd.compare("suspender ") == 0)
      paused = true;
    else if(cmd.compare("restablecer ") == 0)
      paused = false;

    commands_queue.push(cmd);
  }
}

/**
 * Main function
 * all code logic runs here
 */
int main (int argc, char* argv[], char** env) {
  int index; //not used, but can be helpful for debugging
  int o;

  //get options
  do {
    o = getopt_long(argc, argv, "", options, &index);

    switch(o) {
    case -1: //ignore
      break;

    case 'p': //set the path
      path.assign(string(optarg));
      break;

    case 'f': //set the executable
      file.assign(string(optarg));
      break;

    case 'l': //set the number of lives
      lives = atoi(optarg) + 1; //lives = reencarnations + 1
      if(lives == 1) infinite = true;
      else infinite = false;
      break;

    case 's': //set the semaphore
      semaphore.assign(string(optarg));
      break;

    case 'm': //set the sharedmemory
      sharedmem.assign(string(optarg));
      break;

    default: //bad option
      usage(argv[0]);
      exit(1);
      break;
    }
  } while (o != -1);

  //get the id for this control process
  if (argv[optind] == NULL) {
    cerr << argv[0] << ": Mandatory 'id' is missing" << endl;
    usage(argv[0]);
    _exit(1);
  } else {
    id = atoi(argv[optind]);
  }

  //check all non-optional parameters are set
  if (path.empty()) {
    cerr << argv[0] << ": Mandatory 'filepath' is missing" << endl;
    usage(argv[0]);
    _exit(1);
  }

  if (file.empty()) {
    cerr << argv[0] << ": Mandatory 'filename' is missing" << endl;
    usage(argv[0]);
    _exit(1);
  }

  if (lives == -1) {
    cerr << argv[0] << ": Mandatory 'reencarnacion' is missing" << endl;
    usage(argv[0]);
    _exit(1);
  }

  full_path = path+"/"+file;

  initShared();

  thread t(commandReader);

  int status, code, childpid;
  string strlive, cmd;

  do{

    childpid = createChild();

    waitpid(childpid, &status, 0);

    lives--;

    if ( WIFEXITED(status) ) code = WEXITSTATUS(status); // exited normaly
    else if( WIFSIGNALED(status) ) code = WTERMSIG(status); // killed by signal
    else code = -1;

    if(infinite) strlive = "Infinito";
    else strlive = to_string(lives);

    cerr << "Proceso suicida " << info->id << " con PID " << childpid
         << " termino por causa " << code << " -- Proceso Control " << id
         << ", vidas restantes: " << strlive << endl;

    sem_wait(memsem);

    mem->valSeq++;
    info->nDecesos++;

    sem_post(memsem);

    while(!commands_queue.empty()){
      executeCommand(commands_queue.front());
      commands_queue.pop();
    }

  }while(infinite || lives > 0);

  cerr << "terminar" << endl;

  cout << "Proceso Control " << id << " -- "
       << "Proceso suicida " << info->id
       << " termina su ejecucion" << endl;

  t.join();

  destroyShared();

  return 0;
}
